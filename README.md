# scripts-and-scenes-with-starlette

[//]: <> (or provide your own description)
This webapp is designed to allow creation of storyboards
such as a movie script. Recreate your favorite films, books, etc.
or create your own!

This project is built with Starlette, a python api framework.

## data schema

```
stories = [
    {
        _id: ObjectId,
        title: str,
        creator: str, # author, director, whatever
        year_released: int,
        medium: str, # book, movie, tv series, manga, etc.
        characters: [
            {
                _id: ObjectId,
                name: str,
                age: int,
                description: str,
            },
            ...
        ],
        scenes: [
            {
                _id: ObjectId,
                rank: int,
                setting: str,
                plot: str, # dialogue, actions, etc.
                character_ids: list[ObjectId],
            },
            ...
        ],
    },
    ...
]
```

## Getting started

1. create a python virtual environment
2. `pip install -r requirements.txt`
3. `npm install` (needed for automated browser tests written in js)
4. get started!! use each of the following resources (Learn,
    test output, documentation) in unison to complete your project
    - follow along with the instructions in Learn for context
    - use Test Driven Development (TDD) to see and fix the errors
        - `sh -ac '. ./.env; python -m pytest -s'`
        - once those foundational tests are passing you should be in a position
            to run the site with `python -m uvicorn --reload --env-file .env app:app`
            to see your existing UI
        - run the ui tests with `npm run test`
            - note: this one relies on a running server aka getting 3a to pass and step 6
            - these `npm` tests are dependent on data in your DB, for this we've provided:
                - for adding data: `npm run seed`
                - for clearing data: `npm run drop`
    - start with a stub by creating a Starlette app in an app.py file in the root of this project
        - follow the [starlette.io](https://www.starlette.io/) guide for an example
