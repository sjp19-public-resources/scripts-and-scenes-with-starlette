import { storiesUrl, storyDeleteUrlRegex, mockStory } from '../static';


describe('The Story Delete Flow', () => {
    before(() => {
        const commonReqProps = { method: 'POST', form: true, followRedirect: false };
        cy.request({
            ...commonReqProps,
            url: storiesUrl,
            body: mockStory,
        });
    });

    beforeEach(() => {
        cy.visit(storiesUrl);
        cy.get('li a').last().as('lastLink', { type: 'static' });
        cy.get('li a').its('length').as('storyLinksLength', { type: 'static' });
        cy.get('li a').last().click();
    });

    it('has a delete form on the show page', () => {
        cy.get('form')
            .should('have.attr', 'method')
            .should('eq', 'POST');
        cy.get('form')
            .should('have.attr', 'action')
            .should('match', storyDeleteUrlRegex);

        cy.get('form').within(() => {
            cy.get('input[type="submit"]').contains('Delete story');
        });
    });

    it('clicking "Delete story" button removes it from the list stories page', () => {
        cy.contains('Delete story').click();
        cy.url().should('eq', `${Cypress.config().baseUrl}${storiesUrl}`);
        cy.get('@lastLink').should('not.exist');
        cy.get('@storyLinksLength').then(oldLength => {
            if (oldLength === 1) {
                cy.get('li a').should('not.exist');
            } else {
                cy.get('li a').its('length').should('eq' , oldLength - 1);
            }
        });
    });
});
