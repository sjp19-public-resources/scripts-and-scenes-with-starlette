import { storiesUrl, mockScene, sceneShowUrlRegex, mockStory, mockSceneUpdated, sceneUpdateUrlRegex, mockCharacter } from '../static';


describe('The Scene Update Flow', () => {
    before(() => {
        const commonReqProps = { method: 'POST', form: true, followRedirect: false };
        cy.request({
            ...commonReqProps,
            url: storiesUrl,
            body: mockStory,
        }).then(resp => {
            const storyId = resp.headers.location.replace(storiesUrl, '');
            cy.request({
                ...commonReqProps,
                url: `/${storiesUrl}${storyId}/characters`,
                body: mockCharacter,
            }).as('newCharacterResponse');
            cy.request({
                ...commonReqProps,
                url: `/${storiesUrl}${storyId}/scenes`,
                body: mockScene,
            });
        });
    });

    beforeEach(() => {
        cy.visit(storiesUrl);
        cy.get('li a').last().click();
        cy.contains('a', `rank: ${mockSceneUpdated.rank}`).should('not.exist');
        cy.contains('a', `rank: ${mockScene.rank}`).click();
    });

    it('has an update link on the show page', () => {
        cy.contains('update scene data')
            .should('have.attr', 'href')
            .should('match', sceneUpdateUrlRegex);
    });

    it('has an update scene page with a properly built form', () => {
        cy.contains(mockCharacter.name).should('not.exist');

        cy.contains('update scene data').click();
        cy.url().should('match', sceneUpdateUrlRegex);

        cy.get('form')
            .should('have.attr', 'action')
            .should('match', sceneShowUrlRegex);
        cy.get('form')
            .should('have.attr', 'method')
            .should('eq', 'POST');

        cy.get('form').within(() => {
            cy.get('input[name="rank"]').should('have.attr', 'type', 'number');
            cy.get('input[name="setting"]').should('have.attr', 'type', 'text');
            cy.get('textarea[name="plot"]');
            cy.get('input[name="character_ids[]"]')
                .should('have.attr', 'type', 'checkbox');
            cy.contains(mockCharacter.name);
            cy.get('input[type="submit"]').contains('Submit');
        });
    });

    it('submitting the form updates the scene and displays it on the show page', () => {
        cy.contains('update scene data').click();

        cy.get('input[name="rank"]').clear().type(mockSceneUpdated.rank);
        cy.get('input[name="setting"]').clear().type(mockSceneUpdated.setting);
        cy.get('textarea[name="plot"]').clear().type(mockSceneUpdated.plot);
        cy.get('input[name="character_ids[]"]').check();
        cy.get('input[type="submit"]').click();

        cy.url().should('match', sceneShowUrlRegex);

        cy.contains(`rank: ${mockSceneUpdated.rank}`);
        cy.contains(`setting: ${mockSceneUpdated.setting}`);
        cy.contains(`plot: ${mockSceneUpdated.plot}`);
        cy.contains(mockCharacter.name);
    });
});

describe('The Scene Update Flow effects the story show page', () => {
    it('the updated scene also displays on the story show page as a link', () => {
        cy.visit(storiesUrl);
        cy.get('li a').last().click();
        cy.contains('a', `rank: ${mockSceneUpdated.rank}`)
            .should('have.attr', 'href')
            .should('match', sceneShowUrlRegex);
    });
});
