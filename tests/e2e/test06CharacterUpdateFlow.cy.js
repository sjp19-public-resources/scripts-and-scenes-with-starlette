import { storiesUrl, mockCharacter, mockCharacterUpdated, characterShowUrlRegex, characterUpdateUrlRegex, mockStory } from '../static';


describe('The Character Update Flow', () => {
    before(() => {
        const commonReqProps = { method: 'POST', form: true, followRedirect: false };
        cy.request({
            ...commonReqProps,
            url: storiesUrl,
            body: mockStory,
        }).then(resp => {
            const storyId = resp.headers.location.replace(storiesUrl, '');
            cy.request({
                ...commonReqProps,
                url: `/${storiesUrl}${storyId}/characters`,
                body: mockCharacter,
            });
        });
    });

    beforeEach(() => {
        cy.visit(storiesUrl);
        cy.get('li a').last().click();
        cy.contains('a', mockCharacterUpdated.name).should('not.exist');
        cy.contains('a', mockCharacter.name).click();
    });

    it('has an update link on the show page', () => {
        cy.contains('update character data')
            .should('have.attr', 'href')
            .should('match', characterUpdateUrlRegex);
    });

    it('has a update character page with a properly built form', () => {
        cy.contains('update character data').click();
        cy.url().should('match', characterUpdateUrlRegex);

        cy.get('form')
            .should('have.attr', 'action')
            .should('match', characterShowUrlRegex);
        cy.get('form')
            .should('have.attr', 'method')
            .should('eq', 'POST');

        cy.get('form').within(() => {
            cy.get('input[name="name"]').should('have.attr', 'type', 'text');
            cy.get('input[name="age"]').should('have.attr', 'type', 'number');
            cy.get('textarea[name="description"]');
            cy.get('input[type="submit"]').contains('Submit');
        });
    });

    it('submitting the form updates the character and displays it on the show page', () => {
        cy.contains('update character data').click();


        cy.get('input[name="name"]').clear().type(mockCharacterUpdated.name);
        cy.get('input[name="age"]').clear().type(mockCharacterUpdated.age);
        cy.get('textarea[name="description"]').clear().type(mockCharacterUpdated.description);
        cy.get('input[type="submit"]').click();

        cy.url().should('match', characterShowUrlRegex);

        cy.contains(`name: ${mockCharacterUpdated.name}`);
        cy.contains(`age: ${mockCharacterUpdated.age}`);
        cy.contains(`description: ${mockCharacterUpdated.description}`);
    });
});

describe('The Character Update Flow effects the story show page', () => {
    it('the updated character also displays on the story show page as a link', () => {
        cy.visit(storiesUrl);
        cy.get('li a').last().click();
        cy.contains('a', mockCharacterUpdated.name)
            .should('have.attr', 'href')
            .should('match', characterShowUrlRegex);
    });
});
