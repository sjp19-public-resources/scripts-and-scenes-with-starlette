import { storiesUrl, mockStory, mockScene, sceneDeleteUrlRegex, storyShowUrlRegex } from '../static';

describe('The Scene Delete Flow', () => {
    before(() => {
        const commonReqProps = { method: 'POST', form: true, followRedirect: false };
        cy.request({
            ...commonReqProps,
            url: storiesUrl,
            body: mockStory,
        }).then(resp => {
            const storyId = resp.headers.location.replace(storiesUrl, '');
            cy.request({
                ...commonReqProps,
                url: `/${storiesUrl}${storyId}/scenes`,
                body: mockScene,
            });
        });
    });

    beforeEach(() => {
        cy.visit(storiesUrl);
        cy.get('li a').last().click();
        cy.contains('a', `rank: ${mockScene.rank}`).as('sceneLink', { type: 'static' });
        cy.contains('a', `rank: ${mockScene.rank}`).click();
    });

    it('has a delete form on the scene show page', () => {
        cy.get('form')
            .should('have.attr', 'method')
            .should('eq', 'POST');
        cy.get('form')
            .should('have.attr', 'action')
            .should('match', sceneDeleteUrlRegex);

        cy.get('form').within(() => {
            cy.get('input[type="submit"]').contains('Delete scene');
        });
    });

    it('clicking "Delete scene" button removes it from the story show page', () => {
        cy.contains('Delete scene').click();
        cy.url().should('match', storyShowUrlRegex);
        cy.get('@sceneLink').should('not.exist');
    });
});
