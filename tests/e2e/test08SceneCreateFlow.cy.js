import { storiesUrl, mockScene, sceneCreateUrlRegex, scenesUrlRegex, sceneShowUrlRegex, mockStory, mockCharacter, mockCharacterUpdated } from '../static';


describe('The Scene Create Flow', () => {
    before(() => {
        const commonReqProps = { method: 'POST', form: true, followRedirect: false };
        cy.request({
            ...commonReqProps,
            url: storiesUrl,
            body: mockStory,
        }).then(resp => {
            const storyId = resp.headers.location.replace(storiesUrl, '');
            cy.request({
                ...commonReqProps,
                url: `/${storiesUrl}${storyId}/characters`,
                body: mockCharacter,
            });
            // for below, 2 characters needed for the .last checkbox check and .as alias should not exist
            cy.request({
                ...commonReqProps,
                url: `/${storiesUrl}${storyId}/characters`,
                body: mockCharacterUpdated,
            });
        });
    });

    beforeEach(() => {
        cy.visit(storiesUrl);
        cy.get('li a').last().click();
    });

    it('the stories show page has an "add a scene link"', () => {
        cy.contains('add a scene')
            .should('have.attr', 'href')
            .should('match', sceneCreateUrlRegex);
    });

    it('the "add a scene link" takes us to /stories/:id/scenes/create', () => {
        cy.contains('add a scene').click();
        cy.url().should('match', sceneCreateUrlRegex)
    });

    it('has a create scene page with a properly built form', () => {
        cy.contains('add a scene').click();

        cy.get('form').should('have.attr', 'method', 'POST');
        cy.get('form')
            .should('have.attr', 'action')
            .should('match', scenesUrlRegex);
        cy.get('form').within(() => {
            cy.get('input[name="rank"]').should('have.attr', 'type', 'number');
            cy.get('textarea[name="plot"]');
            cy.get('input[name="setting"]').should('have.attr', 'type', 'text');
            cy.get('input[name="character_ids[]"]').last()
                .should('have.attr', 'type', 'checkbox')
                .should('have.attr', 'value');
            cy.get('input[type="submit"]').contains('Submit');
        });
    });

    it('submitting the form creates a new scene and displays it on the show page', () => {
        cy.contains('add a scene').click();

        cy.get('input[name="rank"]').clear().type(mockScene.rank);
        cy.get('input[name="setting"]').clear().type(mockScene.setting);
        cy.get('textarea[name="plot"]').clear().type(mockScene.plot);
        cy.get('input[name="character_ids[]"]').last().check();

        cy.get('label').last().invoke('text').as('checkedCharacterName', { type: 'static' });
        cy.get('label').first().invoke('text').as('uncheckedCharacterName', { type: 'static' });

        cy.get('input[type="submit"]').click();

        cy.url().should('match', sceneShowUrlRegex);

        cy.contains(`rank: ${mockScene.rank}`);
        cy.contains(`setting: ${mockScene.setting}`);
        cy.contains(`plot: ${mockScene.plot}`);
        cy.get('@checkedCharacterName').should('exist');
        cy.get('@uncheckedCharacterName').then(($ele) => {
            cy.contains($ele).should('not.exist');
        });
    });

    it('the new scene also displays on the story show page as a link', () => {
        cy.contains('a', `rank: ${mockScene.rank}`)
            .should('have.attr', 'href')
            .should('match', sceneShowUrlRegex);
    });
});
