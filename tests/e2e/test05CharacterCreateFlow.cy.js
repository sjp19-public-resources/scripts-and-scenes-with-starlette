import { storiesUrl, mockCharacter, characterCreateUrlRegex, charactersUrlRegex, characterShowUrlRegex, mockStory } from '../static';


describe('The Character Create Flow', () => {
    before(() => {
        const commonReqProps = { method: 'POST', form: true, followRedirect: false };
        cy.request({
            ...commonReqProps,
            url: storiesUrl,
            body: mockStory,
        });
    });

    beforeEach(() => {
        cy.visit(storiesUrl);
        cy.get('li a').last().click();
    });

    it('the stories show page has an "add a character link"', () => {
        cy.contains('add a character')
            .should('have.attr', 'href')
            .should('match', characterCreateUrlRegex);
    });

    it('the "add a character link" takes us to /stories/:id/characters/create', () => {
        cy.contains('add a character').click();
        cy.url().should('match', characterCreateUrlRegex)
    });

    it('has a create character page with a properly built form', () => {
        cy.contains('add a character').click();

        cy.get('form').should('have.attr', 'method', 'POST');
        cy.get('form')
            .should('have.attr', 'action')
            .should('match', charactersUrlRegex);
        cy.get('form').within(() => {
            cy.get('input[name="name"]').should('have.attr', 'type', 'text');
            cy.get('input[name="age"]').should('have.attr', 'type', 'number');
            cy.get('textarea[name="description"]');
            cy.get('input[type="submit"]').contains('Submit');
        });
    });

    it('submitting the form creates a new character and displays it on the show page', () => {
        cy.contains('add a character').click();


        cy.get('input[name="name"]').clear().type(mockCharacter.name);
        cy.get('input[name="age"]').clear().type(mockCharacter.age);
        cy.get('textarea[name="description"]').clear().type(mockCharacter.description);
        cy.get('input[type="submit"]').click();

        cy.url().should('match', characterShowUrlRegex);

        cy.contains(`name: ${mockCharacter.name}`);
        cy.contains(`age: ${mockCharacter.age}`);
        cy.contains(`description: ${mockCharacter.description}`);
    });

    it('the new character also displays on the story show page as a link', () => {
        cy.contains('a', mockCharacter.name)
            .should('have.attr', 'href')
            .should('match', characterShowUrlRegex);
    });
});
