import { storiesUrl, mockStoryUpdated, storyShowUrlRegex, storyUpdateUrlRegex, mockStory } from '../static';


describe('The Story Update Flow', () => {
    before(() => {
        const commonReqProps = { method: 'POST', form: true, followRedirect: false };
        cy.request({
            ...commonReqProps,
            url: storiesUrl,
            body: mockStory,
        });
    });

    beforeEach(() => {
        cy.visit(storiesUrl);
        cy.contains('a', mockStoryUpdated.title).should('not.exist');
        cy.get('li a').last().click();
    });

    it('has an update link on the show page', () => {
        cy.contains('update story data')
            .should('have.attr', 'href')
            .should('match', storyUpdateUrlRegex);
    });

    it('has a update story page with a properly built form', () => {
        cy.contains('update story data').click();
        cy.url().should('match', storyUpdateUrlRegex);

        cy.get('form')
            .should('have.attr', 'action')
            .should('match', storyShowUrlRegex);
        cy.get('form')
            .should('have.attr', 'method')
            .should('eq', 'POST');

        cy.get('form').within(() => {
            cy.get('input[name="title"]').should('have.attr', 'type', 'text');
            cy.get('input[name="creator"]').should('have.attr', 'type', 'text');
            cy.get('input[name="year_released"]').should('have.attr', 'type', 'number');
            cy.get('input[name="medium"]').should('have.attr', 'type', 'text');
            cy.get('input[type="submit"]').contains('Submit');
        });
    });

    it('submitting the form updates the story and displays it on the show page', () => {
        cy.contains('update story data').click();

        cy.get('input[name="title"]').clear().type(mockStoryUpdated.title);
        cy.get('input[name="creator"]').clear().type(mockStoryUpdated.creator);
        cy.get('input[name="year_released"]').clear().type(mockStoryUpdated.year_released);
        cy.get('input[name="medium"]').clear().type(mockStoryUpdated.medium);
        cy.get('input[type="submit"]').click();

        cy.url().should('match', storyShowUrlRegex);

        cy.contains(`title: ${mockStoryUpdated.title}`);
        cy.contains(`creator: ${mockStoryUpdated.creator}`);
        cy.contains(`medium: ${mockStoryUpdated.medium}`);
        cy.contains(`year released: ${mockStoryUpdated.year_released}`);
    });
});

describe('The Story Update Flow effects the list page', () => {
    it('the updated story also displays on the list page as a link', () => {
        cy.visit(storiesUrl);
        cy.contains('a', mockStoryUpdated.title)
            .should('have.attr', 'href')
            .should('match', storyShowUrlRegex);
    });
});
