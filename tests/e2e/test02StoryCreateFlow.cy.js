import { storiesUrl, storyCreateUrl, mockStory, storyShowUrlRegex } from '../static';


describe('The Story Create Flow', () => {
    beforeEach(() => {
        cy.visit(storyCreateUrl);
    });

    it('has a create story page with a properly built form', () => {
        cy.get('form').should('have.attr', 'method', 'POST');
        cy.get('form').should('have.attr', 'action', storiesUrl);
        cy.get('form').within(() => {
            cy.get('input[name="title"]').should('have.attr', 'type', 'text');
            cy.get('input[name="creator"]').should('have.attr', 'type', 'text');
            cy.get('input[name="year_released"]').should('have.attr', 'type', 'number');
            cy.get('input[name="medium"]').should('have.attr', 'type', 'text');
            cy.get('input[type="submit"]').contains('Submit');
        });
    });

    it('submitting the form creates a new story and displays it on the show page', () => {
        cy.get('input[name="title"]').type(mockStory.title);
        cy.get('input[name="creator"]').type(mockStory.creator);
        cy.get('input[name="year_released"]').type(mockStory.year_released);
        cy.get('input[name="medium"]').type(mockStory.medium);
        cy.get('input[type="submit"]').click();

        cy.url().should('match', storyShowUrlRegex);

        cy.contains(`title: ${mockStory.title}`);
        cy.contains(`creator: ${mockStory.creator}`);
        cy.contains(`medium: ${mockStory.medium}`);
        cy.contains(`year released: ${mockStory.year_released}`);
    });
});

describe('The Story Create Flow effects the list page', () => {
    it('the new story also displays on the list page as a link', () => {
        cy.visit(storiesUrl);
        cy.contains('a', mockStory.title)
            .should('have.attr', 'href')
            .should('match', storyShowUrlRegex);
    });
});
