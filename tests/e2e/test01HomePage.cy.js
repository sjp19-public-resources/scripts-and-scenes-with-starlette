import { storiesUrl, storyCreateUrl } from '../static';


describe('The Home Page', () => {
    beforeEach(() => {
        cy.visit('/');
    });

    it('successfully loads', () => {
        cy.request('/').then((response) => {
            expect(response.status).to.eq(200);
        });
    });
    
    it('has an h1 for the app\'s name with class app_name', () => {
        cy.get('h1.app_name').should('contain', 'Scripts and scenes');
    });
    
    it('has a nav bar with link tags to home, stories list, and new story form', () => {
        cy.get('div.nav');
        cy.get('div.nav a[href="/"]').contains('home');
        cy.get(`div.nav a[href="${storiesUrl}"]`).contains('stories list');
        cy.get(`div.nav a[href="${storyCreateUrl}"]`).contains('new story form');
    });

    it('nav bar home link works', () => {
        cy.get('div.nav a[href="/"]').click();
        cy.url().should('eq', Cypress.config().baseUrl + '/')
    });

    it('nav bar stories list link works', () => {
        cy.get(`div.nav a[href="${storiesUrl}"]`).click();
        cy.url().should('eq', `${Cypress.config().baseUrl}${storiesUrl}`)
    });

    it('nav bar new story form link works', () => {
        cy.get(`div.nav a[href="${storyCreateUrl}"`).click();
        cy.url().should('eq', `${Cypress.config().baseUrl}${storyCreateUrl}`)
    });
});
