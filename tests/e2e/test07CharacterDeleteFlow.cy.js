import { storiesUrl, mockCharacter, characterDeleteUrlRegex, storyShowUrlRegex, mockStory } from '../static';


describe('The Character Delete Flow', () => {
    before(() => {
        const commonReqProps = { method: 'POST', form: true, followRedirect: false };
        cy.request({
            ...commonReqProps,
            url: storiesUrl,
            body: mockStory,
        }).then(resp => {
            const storyId = resp.headers.location.replace(storiesUrl, '');
            cy.request({
                ...commonReqProps,
                url: `/${storiesUrl}${storyId}/characters`,
                body: mockCharacter,
            });
        });
    });

    beforeEach(() => {
        cy.visit(storiesUrl);
        cy.get('li a').last().click();
        cy.contains('a', mockCharacter.name).as('characterLink', { type: 'static' });
        cy.contains('a', mockCharacter.name).click();
    });

    it('has a delete form on the character show page', () => {
        cy.get('form')
            .should('have.attr', 'method')
            .should('eq', 'POST');
        cy.get('form')
            .should('have.attr', 'action')
            .should('match', characterDeleteUrlRegex);

        cy.get('form').within(() => {
            cy.get('input[type="submit"]').contains('Delete character');
        });
    });

    it('clicking "Delete character" button removes it from the story show page', () => {
        cy.contains('Delete character').click();
        cy.url().should('match', storyShowUrlRegex);
        cy.get('@characterLink').should('not.exist');
    });
});
