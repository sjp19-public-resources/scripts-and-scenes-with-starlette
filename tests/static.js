const { v4: uuidv4 } = require('uuid');

const mockStory = {
    title: uuidv4(),
    creator: 'James',
    year_released: 1990,
    medium: 'life',
};

const mockStoryUpdated = {
    title: uuidv4(),
    creator: 'James R.',
    year_released: 2023,
    medium: 'test code',
};

const mockCharacter = {
    name: uuidv4(),
    age: 33,
    description: 'description start',
};

const mockCharacterUpdated = {
    name: uuidv4(),
    age: 33,
    description: 'description updated',
};

const mockScene = {
    rank: 1,
    plot: uuidv4(),
    setting: 'the place',
    // character_ids (bson ObjectIds) added as needed in tests
    // 'character_ids[]': character_id,
};

const mockSceneUpdated = {
    rank: 2,
    plot: uuidv4(),
    setting: 'the new place',
    // character_ids (bson ObjectIds) added as needed in tests
    // 'character_ids[]': character_id,
};

const storiesUrl = '/stories';
const storyCreateUrl = '/stories/create';
const storyShowUrlRegex = /stories\/(\d|[a-zA-Z]){24}/;
const storyUpdateUrlRegex = /stories\/(\d|[a-zA-Z]){24}\/update/;
const storyDeleteUrlRegex = /stories\/(\d|[a-zA-Z]){24}\/delete/;
const charactersUrlRegex = /stories\/(\d|[a-zA-Z]){24}\/characters/;
const characterCreateUrlRegex = /stories\/(\d|[a-zA-Z]){24}\/characters\/create/;
const characterShowUrlRegex = /stories\/(\d|[a-zA-Z]){24}\/characters\/(\d|[a-zA-Z]){24}/;
const characterUpdateUrlRegex = /stories\/(\d|[a-zA-Z]){24}\/characters\/(\d|[a-zA-Z]){24}\/update/;
const characterDeleteUrlRegex = /stories\/(\d|[a-zA-Z]){24}\/characters\/(\d|[a-zA-Z]){24}\/delete/;
const scenesUrlRegex = /stories\/(\d|[a-zA-Z]){24}\/scenes/;
const sceneCreateUrlRegex = /stories\/(\d|[a-zA-Z]){24}\/scenes\/create/;
const sceneShowUrlRegex = /stories\/(\d|[a-zA-Z]){24}\/scenes\/(\d|[a-zA-Z]){24}/;
const sceneUpdateUrlRegex = /stories\/(\d|[a-zA-Z]){24}\/scenes\/(\d|[a-zA-Z]){24}\/update/;
const sceneDeleteUrlRegex = /stories\/(\d|[a-zA-Z]){24}\/scenes\/(\d|[a-zA-Z]){24}\/delete/;

module.exports = {
    mockStory,
    mockStoryUpdated,
    mockCharacter,
    mockCharacterUpdated,
    mockScene,
    mockSceneUpdated,
    storiesUrl,
    storyCreateUrl,
    storyShowUrlRegex,
    storyUpdateUrlRegex,
    storyDeleteUrlRegex,
    charactersUrlRegex,
    characterCreateUrlRegex,
    characterShowUrlRegex,
    characterUpdateUrlRegex,
    characterDeleteUrlRegex,
    scenesUrlRegex,
    sceneCreateUrlRegex,
    sceneShowUrlRegex,
    sceneUpdateUrlRegex,
    sceneDeleteUrlRegex,
}
