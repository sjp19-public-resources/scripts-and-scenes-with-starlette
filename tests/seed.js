const MongoClient = require("mongodb").MongoClient;
const { ObjectId } = require("bson");
const {
    mockStory,
    mockStoryUpdated,
    mockCharacter,
    mockCharacterUpdated,
    mockScene,
    mockSceneUpdated,
} = require('./static');


async function seedDB() {
    const client = new MongoClient(process.env.MONGODB_URL);

    try {
        await client.connect();
        console.log("Connected to server");

        const db = client.db(process.env.DATABASE_NAME);

        const charId1 = `${new ObjectId()}`;
        const charId2 = `${new ObjectId()}`;
        const characters = [
            {
                ...mockCharacter,
                _id: charId1,
            },
            {
                ...mockCharacterUpdated,
                _id: charId2,
            },
        ];
        const stories = [
            {
                ...mockStory,
                _id: `${new ObjectId()}`,
                character_ids: [charId1],
                scenes: [{
                    ...mockScene,
                    _id: `${new ObjectId()}`,
                }],
            },
            {
                ...mockStoryUpdated,
                _id: `${new ObjectId()}`,
                character_ids: [charId2],
                scenes: [{
                    ...mockSceneUpdated,
                    _id: `${new ObjectId()}`,
                }],
            },
        ];

        await db.collection('CHARACTERS').insertMany(characters);
        await db.collection('STORIES').insertMany(stories);
        console.log("Data seeded");
        client.close();
    } catch (err) {
        console.log(err.stack);
    }
}

seedDB();
