from bson import ObjectId
import os


def test_file_structure():
    assert os.path.isfile("./app.py"), "an app.py file exists in the root"
    assert os.path.isfile(
        "./templates/base.html"
    ), "a base.html template file exists in the templates dir"
    assert os.path.isfile(
        "./data_access/db_connection.py"
    ), "a db_connection.py file exists in the data_access dir"

    # stories
    assert os.path.isfile(
        "./controllers/stories.py"
    ), "a stories.py file exists in the controllers directory"
    assert os.path.isfile(
        "./models/stories.py"
    ), "a stories.py file exists in the models directory"
    assert os.path.isfile(
        "./routes/stories.py"
    ), "a stories.py file exists in the routes directory"
    assert os.path.isfile(
        "./data_access/stories.py"
    ), "a stories.py file exists in the data_access dir"

    assert os.path.isfile(
        "./templates/stories/form.html"
    ), "a form template file exists in the templates/stories directory"
    assert os.path.isfile(
        "./templates/stories/list.html"
    ), "a list template file exists in the templates/stories directory"
    assert os.path.isfile(
        "./templates/stories/show.html"
    ), "a show template file exists in the templates/stories directory"
    assert os.path.isfile(
        "./templates/stories/update.html"
    ), "an update template file exists in the templates/stories directory"

    # characters
    assert os.path.isfile(
        "./controllers/characters.py"
    ), "a characters.py file exists in the controllers directory"
    assert os.path.isfile(
        "./models/characters.py"
    ), "a characters.py file exists in the models directory"
    assert os.path.isfile(
        "./routes/characters.py"
    ), "a characters.py file exists in the routes directory"
    assert os.path.isfile(
        "./data_access/characters.py"
    ), "a characters.py file exists in the data_access dir"

    assert os.path.isfile(
        "./templates/characters/form.html"
    ), "a form template file exists in the templates/characters directory"
    assert os.path.isfile(
        "./templates/characters/show.html"
    ), "a show template file exists in the templates/characters directory"
    assert os.path.isfile(
        "./templates/characters/update.html"
    ), "an update template file exists in the templates/characters directory"

    # scenes
    assert os.path.isfile(
        "./controllers/scenes.py"
    ), "a scenes.py file exists in the controllers directory"
    assert os.path.isfile(
        "./models/scenes.py"
    ), "a scenes.py file exists in the models directory"
    assert os.path.isfile(
        "./routes/scenes.py"
    ), "a scenes.py file exists in the routes directory"
    assert os.path.isfile(
        "./data_access/scenes.py"
    ), "a scenes.py file exists in the data_access dir"

    assert os.path.isfile(
        "./templates/scenes/form.html"
    ), "a form template file exists in the templates/scenes directory"
    assert os.path.isfile(
        "./templates/scenes/show.html"
    ), "a show template file exists in the templates/scenes directory"
    assert os.path.isfile(
        "./templates/scenes/update.html"
    ), "an update template file exists in the templates/scenes directory"


def test_root_imports():
    try:
        from data_access.db_connection import db

        assert (
            type(db).__name__ == "Database"
        ), "the db variable is a MongoClient Database instance"
    except ImportError:
        assert False, "a db variable must exist in db_connection.py"

    try:
        from app import app

        assert (
            type(app).__name__ == "Starlette"
        ), "the app variable is an instance of a Starlette server"
    except ImportError:
        assert False, "an app variable must exist in app.py"


def test_story_imports():
    try:
        from routes.stories import routes
    except ImportError:
        assert False, "a routes variable must exist in routes/stories.py"

    assert type(routes).__name__ == "list"

    try:
        from models.stories import Story
    except ImportError:
        assert False, "an Story class must exist in models/stories.py"


def test_story_model():
    from models.stories import Story

    story_data = {
        "title": "test",
        "year_released": 2023,
        "creator": "james",
        "medium": "book",
        "extra": 1,
    }
    story = Story(**story_data)
    assert story.title == story_data["title"]
    assert story.year_released == story_data["year_released"]
    assert story.creator == story_data["creator"]
    assert story.medium == story_data["medium"]
    assert story._id is not None, "creates a default _id when none is given"
    try:
        assert story.extra is None
    except AttributeError as ae:
        assert (
            str(ae) == "'Story' object has no attribute 'extra'"
        ), "the Story class must ignore any extra parameters passed in to init"

    story_id = str(ObjectId())
    story = Story(**story_data, _id=story_id)
    assert story._id == story_id, "can also take in an _id"


def test_character_imports():
    try:
        from routes.characters import routes
    except ImportError:
        assert False, "a routes variable must exist in routes/characters.py"

    assert type(routes).__name__ == "list"

    try:
        from models.characters import Character
    except ImportError:
        assert False, "a Character class must exist in models/characters.py"


def test_character_model():
    from models.characters import Character

    character_data = {
        "name": "james",
        "age": 33,
        "description": "instructor",
        "extra": 1,
    }
    character = Character(**character_data)
    assert character.name == character_data["name"]
    assert character.age == character_data["age"]
    assert character.description == character_data["description"]
    assert character._id is not None, "creates a default _id when none is given"
    try:
        assert character.extra is None
    except AttributeError as ae:
        assert (
            str(ae) == "'Character' object has no attribute 'extra'"
        ), "the Character class must ignore any extra parameters passed in to init"

    char_id = str(ObjectId())
    character = Character(**character_data, _id=char_id)
    assert character._id == char_id, "can also take in an _id"


def test_scene_imports():
    try:
        from routes.scenes import routes
    except ImportError:
        assert False, "a routes variable must exist in routes/scenes.py"

    assert type(routes).__name__ == "list"

    try:
        from models.scenes import Scene
    except ImportError:
        assert False, "a Scene class must exist in models/scenes.py"


def test_scene_model():
    from models.scenes import Scene

    scene_data = {
        "rank": 1,
        "plot": "some stuff happened",
        "setting": "that place",
        "character_ids": [ObjectId()],
        "extra": 1,
    }
    scene = Scene(**scene_data)
    assert scene.rank == scene_data["rank"]
    assert scene.plot == scene_data["plot"]
    assert scene.setting == scene_data["setting"]
    assert scene.character_ids == scene_data["character_ids"]
    assert scene._id is not None, "creates a default _id when none is given"
    try:
        assert scene.extra is None
    except AttributeError as ae:
        assert (
            str(ae) == "'Scene' object has no attribute 'extra'"
        ), "the Scene class must ignore any extra parameters passed in to init"

    scene_id = str(ObjectId())
    scene = Scene(**scene_data, _id=scene_id)
    assert scene._id == scene_id, "can also take in an _id"
