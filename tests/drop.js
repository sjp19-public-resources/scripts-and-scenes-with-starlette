const MongoClient = require("mongodb").MongoClient;


async function dropDB() {
    const client = new MongoClient(process.env.MONGODB_URL);

    try {
        await client.connect();
        console.log("Connected to server");

        const db = client.db(process.env.DATABASE_NAME);

        await db.collection('STORIES').drop();
        await db.collection('CHARACTERS').drop();
        console.log("Data dropped");
        client.close();
    } catch (err) {
        console.log(err.stack);
    }
}

dropDB();
